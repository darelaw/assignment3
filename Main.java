/*
 * Here is the Main class for the room code.
 * This class contains three rooms.
 * Each rooms has different attributes and design.
 * 
 */
public class Main {
	
     // Here is the main method of the argument. 
	
	public static void main(String[] args) {
        
         // description about the first room.
		Room first = new Room("hardwood","yellow", 1);
		first.getWalls();
		System.out.println(first);
		first.setFloors("hardwood");
		first.setWindows (1);
        
		// Second room description goes here.
        Room second = new Room ("tiled","purple", 0);
		second.getFloors();
		System.out.println(second);
		first.setFloors("tiled");
		first.setWindows(0);

         // Third room description goes here. 
		Room third = new Room("carpeted","white", 3);
		third.getWindows();
		System.out.println(third);
		first.setFloors("carpet");
		first.setWindows(3);
	}
}
